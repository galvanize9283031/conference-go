import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_pic(city, state):
    # get a pic for the place
    response = requests.get("https://api.pexels.com/v1/search",
                            params={
                                "query": "city of " + city + "," + state,
                                "per_page": 1,
                            },
                            headers={"Authorization": PEXELS_API_KEY})
    try:
        vals = response.json()
    except requests.JSONDecodeError:
        return {"picture_url": None}
    url = vals['photos'][0]['url']
    return {"picture_url": url}


def get_weather_data(city, state):
    # Use the Open Weather API
    # Get coordinates
    rr = requests.get("http://api.openweathermap.org/geo/1.0/direct",
                      params={
                          "q": city,
                          "appid": OPEN_WEATHER_API_KEY,
                          "limit": 1,
                      }).json()
    if 'cod' in rr:
        # got an error, return empty data
        return None
    lat, lon = rr[0]["lat"], rr[0]["lon"]
    # get weather for the given coordinates
    rr = requests.get("https://api.openweathermap.org/data/2.5/weather",
                      params={
                          "lat": lat,
                          "lon": lon,
                          "appid": OPEN_WEATHER_API_KEY,
                          "units": "imperial",
                      }).json()
    if rr['cod'] != 200:
        # got an error, return empty data
        return None
    temp, desc = rr["main"]["temp"], rr["weather"][0]["main"]
    print(temp, desc)
    return {"temp": temp, "description": desc}
