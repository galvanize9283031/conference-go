from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DateEncoder(JSONEncoder):
    """
    Encode date as JSON string using the ISO format.
    """
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            out = {}
            if hasattr(o, "get_api_url"):
                out["href"] = o.get_api_url()
            for property in self.properties:
                val = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    val = encoder.default(val)
                out[property] = val
            out.update(self.get_extra_data(o))
            return out
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
